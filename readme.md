# My Vim Config

## Setup

1. `git clone https://gitlab.com/andrew.womeldorf/vim.git ~/.vim` - Clone this
   repo
2. `ln -s ~/.vim/vimrc ~/.vimrc` Create a symlink to the version controlled
   `vimrc` in your home directory
3. `git clone https://github.com/VundleVim/Vundle.vim.git
   ~/.vim/bundle/Vundle.vim` Install [Vundle](https://github.com/VundleVim/Vundle.vim)
4. `vim +PluginInstall +qall` Install plugins

## Modifications

If you don't want or need all of the config settings, change the `SourceDirectory()` calls to `SourceIfExists()` for each config file.

## Plugins

Plugin handling is done with [Vundle](https://github.com/VundleVim/Vundle.vim).

## TODO

- [ ] Use the [native Vim8 Package handler](https://begriffs.com/posts/2019-07-19-history-use-vim.html#third-party-plugins).
- [ ] Find a better way to manage plugins, since I don't need all of them all the time.
