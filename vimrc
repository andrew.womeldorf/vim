" imp Warning Fix https://github.com/vim/vim/issues/3117
if has('python3')
    silent! python3 1
endif


set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

" Terraform
Plugin 'hashivim/vim-terraform'

" Home Grown
Plugin 'https://gitlab.com/andrew.womeldorf/Foldstack.vim'
Plugin 'https://gitlab.com/andrew.womeldorf/rst-table.vim'

" Tmux Navigation
Plugin 'christoomey/vim-tmux-navigator'

" Easy Comments
Plugin 'scrooloose/nerdcommenter'

" Easy File Nav
Plugin 'ctrlpvim/ctrlp.vim'

" File Map
Plugin 'majutsushi/tagbar'

" Align text by character
Plugin 'godlygeek/tabular'

" DBGp catcher
Plugin 'vim-vdebug/vdebug'

" Color Schemes http://vimcolors.com/
Plugin 'KeitaNakamura/neodark.vim'

"snippets: vim-addon-mw-utils and tlib_vim are required dependencies of vim-snippets
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
"Plugin 'garbas/vim-snipmate'

" Editor Config
Plugin 'editorconfig/editorconfig-vim'

" Ack
Plugin 'mileszs/ack.vim'

" Go
Plugin 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" React
Plugin 'mxw/vim-jsx'
Plugin 'pangloss/vim-javascript'
Plugin 'vim-syntastic/syntastic'

" Python
Plugin 'davidhalter/jedi-vim'

call vundle#end()
filetype plugin indent on

" Modularized vimrc
" https://devel.tech/snippets/n/vIIMz8vZ/load-vim-source-files-only-if-they-exist/
" https://devel.tech/snippets/n/vIMvi29n/include-all-vim-files-in-a-directory/
function! SourceIfExists(file)
    if filereadable(expand(a:file))
        exe 'source' a:file
    endif
endfunction

function! SourceDirectory(file)
  for s:fpath in split(globpath(a:file, '*.vim'), '\n')
    exe 'source' s:fpath
  endfor
endfunction

call SourceDirectory('~/.vim/settings')
call SourceDirectory('~/.vim/settings/plugins')

filetype plugin on
