" Save folds when closing and load when opening
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview
