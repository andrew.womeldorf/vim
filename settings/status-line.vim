"""""""""""""""""""""""""""""""""""""""""""""""""""
" STATUS LINE
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Always show ethe status line
set laststatus=2

" Format the status line
set statusline=%f\ Line:\ %l/%L\ \ Column:\ %c
