" Set leader to the comma
let mapleader = ","
let g:mapleader = ","

" Put swap files in a separate directory
set directory=$HOME/.vim/swapfiles/

" Add line numbers
set number

set encoding=utf-8

" Always show a minimum of 3 lines at the top/bottom from where the cursor is
set scrolloff=3

" When a file has been detected to have been changed outside of Vim, read it again
set autoread

" ignore case when the pattern contains lowercase letters only
set ignorecase

" helps make regex patterns behave normally
set magic

" highlight the pattern being searched for while typing
set incsearch

" show the matching bracket
set showmatch

" How many tenths of a second to blink when matching brackets
set mat=2

" auto indent
set ai

" smart indent
set si
