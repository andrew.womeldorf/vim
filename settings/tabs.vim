"-------------------- TABBING --------------------"
" The width of a hard tabstop measured in 'spaces' --- effectively the
" (maximum) width of an actual tab character.
set tabstop=4

" The size of an 'indent'. It's also measured in spaces, so if your code base
" indents with tab characters then you want `shiftwidth` to equal the number
" of tab characters times `tabstop`.  This is also used by things like `=`,
" `>` and `<` commands.
set shiftwidth=4

" Setting this to a non-zero value other than `tabstop` will make the tab key
" (in insert mode) insert a combination of spaces (and possibly tabs) to
" simulate tab stops at this width.
set softtabstop=4

" Enabling this will make the tab key (in insert mode) insert spaces instead
" of tab characters.  This also affects the behavior of the `retab` command.
set expandtab

" Enabling this will make the tab key (in insert mode) insert spaces or tabs
" to go to the next indent of the next tabstop when the cursor is at the
" beginning of a line (i.e. the only preceding characters are whitespace)
set nosmarttab

"----------------- FILETYPE TABS -----------------"
" javascript - StandardJS
autocmd FileType javascript setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
autocmd FileType json setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
autocmd FileType vue setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab

" php - PSR2
autocmd FileType php setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab

" scss
autocmd FileType scss setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab

" yaml
autocmd FileType yaml setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab

" python
autocmd FileType py setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=79 expandtab autoindent fileformat=unix

" Makefile
autocmd FileType make setlocal noexpandtab tabstop=8 shiftwidth=8 softtabstop=0
