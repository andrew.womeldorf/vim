"---------------- RST Table ----------------"
map <leader><leader>c :RSTTableCreate<CR>
map <leader><leader>h :RSTTableCreateWithHeader<CR>
map <leader><leader>f :RSTTableFormat<CR>
map <leader><leader>r :RSTTableRemove<CR>
