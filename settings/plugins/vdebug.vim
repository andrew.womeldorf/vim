"-------------------- VDEBUG --------------------"
" DI WP Docker
let g:vdebug_options={
\    'port': 9005,
\    'path_maps': {'/var/www': '/Volumes/Code/dealerinspire/dealerinspire-core'},
\    'break_on_open': 1
\}
